#!/bin/bash

book="$1"

rm -f total_count.txt

for kind in basic functional descriptive; do
    while read verb; do
        count=$(rg -c "[,;!?—]” ([a-z]* )*${verb}\b" "$book")
        echo $count $verb
    done < verbs_${kind}.txt | sort -n -r > count_${kind}.txt
done

rm -f adverbs.txt
for kind in basic functional descriptive; do
    while read verb; do
        rg -o '[,;!?—]” ([a-z]* )*'"${verb}"' [^“”.,;]*\b[a-z]+ly\b' "$book" | rg -v -w '[a-z]*[^l][^y] and [a-z]*[^l][^y]' | rg -v -w 'as' | rg -o -w '[a-z]*ly' >> adverbs.txt
    done < verbs_${kind}.txt
done
sort adverbs.txt | uniq -c | sort -r -n > adverb_count.txt
wc -l adverbs.txt > all_adverb_count.txt

for kind in basic functional descriptive; do
    while read verb; do
        count=$(rg -c '^([^”]|[^,;!?—’]”)*\b'"${verb}"'(, )?[A-Za-z ]*[,:] “' "$book")
        echo $count $verb
    done < verbs_${kind}.txt | sort -n -r > prefix_count_${kind}.txt
done

for kind in basic functional descriptive; do
    while read verb; do
        count=$(rg -c --multiline ' '"${verb}"'\b(, )?[A-Za-z ]*[,:—]\n\n“[^‘]' "$book")
        echo $count $verb
    done < verbs_${kind}.txt | sort -n -r > prefix_eol_count_${kind}.txt
done

for kind in basic functional descriptive; do
    while read verb; do
        count=$(rg --no-filename $verb count_*.txt prefix_count_*.txt prefix_eol_count_*.txt | rg -o '\s*[0-9]+ ' | awk '{ sum += $1 } END { print sum }')
        echo $count $verb
    done < verbs_${kind}.txt | sort -n -r > all_count_${kind}.txt
    awk '{ sum += $1 } END { print "'${kind}'", sum }' all_count_${kind}.txt >> total_count.txt
done


# No Tag, Prefix Tag, or Action Beat:
# Must not contain a postfix dialogue tag
#   * Ends with .” !” or ?” followed by any amount of text as long as it:
#       * Begins with a capital letter and
#       * Doesn't contain “
#   * Prefix does not contain ,” !” or ?” (i.e. things that could precede a dialogue tag)
rg -c '^([^”]+|[^,;!?—”]”)*“[^”]+[.!?]”( [A-Z][^“]*)?$' "$book" > maybe_no_tag_count.txt

# No Tag:
# Must contain exactly one dialogue fragment
#   * Ends with .” !” or ?”
#   * Begins with “
#   * Interior of line does not contain .” ,” !” or ?” (i.e. things that could be a break in a quote)
rg -c --pcre2 --multiline '(?<=(?<!:)\n\n)“[^”\n]*[.!?]”$' "$book" > no_count.txt

# Prefix Tag:
# Preceded by dialogue tag and colon
#   * Ends with , “ or : “
#   * Prefix does not contain ,” !” or ?” (i.e. things that could precede a dialogue tag)
rg -c '^([^”]|[^,;!?—’]”)*[,:] “' "$book" > prefix_count.txt

# Prefix tag and end of line (for a quote that begins on the next line):
rg -c --multiline '[,:—]\n\n“[^‘]' "$book" > prefix_eol_count.txt

# Action Beat:
# Sequence of dialogue fragments, as long as there are no dialogue tags
#   * Ends with .” !” or ?” followed by non-empty text as long as it:
#       * Begins with a capital letter and
#       * Doesn't contain “
#   * Interior does not contain .” ,” !” or ?” (i.e. things that could precede a dialogue tag or fragment)
#   * Begins with “

rg -c --pcre2 --multiline '(?<=(?<!:)\n\n)“[^”]+[.!?]” [A-Z][^“]+[^:]$' "$book" > action_beat_count.txt

# Multiple Action Beats:
# Sequence of dialogue fragments, as long as there are no dialogue tags
#   * Ends with .” !” or ?” followed by any amount of text as long as it:
#       * Begins with a capital letter and
#       * Doesn't contain “
#   * Last dialogue fragment begins with “ and is not preceded by :
#   * Any number of dialogue fragments bounded by “” as long as they are not preceded by :
rg -c --pcre2 --multiline '(?<=(?<!:)\n\n)(([^”]*[^,:] )?“[^”]+[.]”)*[^”]*[^,:] “[^”]+[.!?]”( [A-Z][^“]*[^:])?$' "$book" > multi_action_beat_count.txt

# FIXME: What about a combination of action beat and prefix tag

# Summary

standard_including_adverbs=$(awk '{ sum += $1 } END { print sum }' count_*.txt)
adverb=$(awk '{ sum += $1 } END { print sum }' adverb_count.txt)
standard=$(( standard_including_adverbs - adverb ))
prefix=$(rg --no-filename -o '\s*[0-9]+ ' prefix*_count_*.txt | awk '{ sum += $1 } END { print sum }')
none=$(cat no_count.txt)
action_beat=$(( $(cat action_beat_count.txt) + $(cat multi_action_beat_count.txt) ))

cat > summary.txt <<EOF
standard $standard
adverb $adverb
prefix $prefix
no $none
action_beat $action_beat
EOF
