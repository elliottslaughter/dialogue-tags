#!/bin/bash

book="$1"

rg -o '[,!?]”\s+[a-z]\w+\s+\w+\b' "$book" | rg -o '\b\w+\b' | sort -u | rg '^[a-z]' > verbs_raw.txt

# Manually read through verbs_raw.txt to make verbs.txt
