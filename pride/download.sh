#!/bin/bash

# Note: use the ePub version because the plain text from Project
# Gutenberg has soft-wrapped lines.

# These are slow, do them once.
if [[ ! -e pride.epub ]]; then
    curl -L -o pride.epub https://www.gutenberg.org/ebooks/1342.epub.noimages
fi
if [[ ! -e pride_calibre.txt ]]; then
    ebook-convert pride.epub pride_calibre.txt
fi

# The line number where the actual text starts and stops.
start=$(grep -n 'Chapter 1$' pride_calibre.txt | cut -f1 -d: | head -n2 | tail -n 1)
stop=$(grep -n 'End of the Project Gutenberg EBook' pride_calibre.txt | cut -f1 -d: | tail -n 1)

sed -n "${start},${stop}p" pride_calibre.txt > pride.txt
