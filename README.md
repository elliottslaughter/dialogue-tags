This repository contains instructions and scripts for replicating the
analysis from my [empirical study of dialogue
tags](https://elliottslaughter.com/2020/06/dialogue-tags), as well as
the raw results from said study.

Note that the instructions below assume familiarity with the command
line. In addition, if you wish to inspect or modify the analysis
itself, you will need to understand shell scripting and regular
expressions.

Important: both this README, and the scripts in this repo, and encoded
in UTF-8. Characters like open and close curly quotes are encoded
directly as Unicode characters. This is important as it helps me keep
my sanity, but it's important to view this file under an editor that
can handle these characters.

# Basic Approach

This analysis relies heavily on regular expressions to identify
certain forms of dialogue tags. The key insight that enables this
approach is that, at least in the British style of dialogue tags, all
tags will take the form of pronoun-verb (e.g., `,” she said`) or else
verb-noun phrase (e.g. `,” said Bob the plumber`). In particular,
because know the verb will either be in first or second position, we
don't need to parse a (potentially) arbitrarily long noun phrase---and
can thus avoid a lot of complex and potentially finicky natural
language processing.

The rules for other forms are similar, e.g. for prefix tags we look
for something that looks vaguely like `said: “`. There are some
complex rules to make sure that any given quote matches as at most one
kind of tag.

Inevitably though, regular expressions are brittle, and this means
that the analysis presented in this repo is tuned *specifically for
the books I identified in the study*. I extensively checked the
results of the analysis for each of the books to verify that it was
behaving itself well, but I *make no guarantees that it will work
well for other books*. If you intend to use it, I recommend you
inspect the results closely to make sure the analysis is giving you
the intended results.

# Replicating the Analysis

## Prerequisites

  * Calibre
  * Ripgrep with PCRE2 support

On macOS you can install these tools with [Homebrew](https://brew.sh/):

```
brew install ripgrep
brew cask install calibre
```

## Methodology: The Fast Version

Use these instructions if you just want to replicate the analysis in
the book.

First, you need to convert the ebooks into an appropriate format. For
each ebook, go to the appropriate directory and run the commands
below. (Note: for *Pride and Prejudice* these steps are unnecessary as
the files are committed directly into the repo.)

 1. Copy epub file to `$book.epub` (e.g. `lotr.epub`).

 2. Run conversion script `./convert.sh`. This will generate
    `$book.txt` (e.g. `lotr.txt`).

After this is finished, you can go back to the top-level repo and run
`./extract_all.sh`. This will run the `extract.sh` scripts in each of
the subdirectories.

## Methodology: The Complete Version

If you want to thoroughly replicate my results, or want to do the
analysis for a new book, the follow additional steps are
necessary. Complete steps (1-2) as above, and then do:

 3. Run verb extraction script `./count.sh`. This will generate `verbs_raw.txt`.

 4. Manually copy `verbs_raw.txt` to `verbs.txt` and filter out verbs
    that are not part of a dialogue tag. If you want to check how a verb
    is used in context, run (e.g., for "wept"):

        $ rg -o '[,!?]’\s+[a-z]\w+\s+\w+\b' lotr.txt | rg wept
        ,’ he wept
        $ rg ',’ he wept' lotr.txt
        20588:‘Don’t kill us,’ he wept. ‘Don’t hurt us with nassty cruel steel! Let us live, yes, live just a little longer. Lost lost! We’re lost. And when Precious goes we’ll die, yes, die into the dust.’ He clawed up the ashes of the path with his long fleshless fingers. ‘Dusst!’ he hissed.

 5. Manually split `verbs.txt` into `verbs_basic.txt`,
    `verbs_functional.txt` and `verbs_descriptive.txt`.

 6. Run `./extract.sh` to perform the rest of the automated analysis.

 7. To check the results of the automated analysis, take individual
    commands from `extract.sh`, remove the `-c` flag, and run
    them. All matches will be displayed.
