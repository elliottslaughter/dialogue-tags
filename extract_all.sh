#!/bin/bash

for dir in lotr pride hp1 president; do
    if [[ -e $dir/$dir.epub ]]; then
        pushd $dir
        ./extract.sh
        popd
    else
        echo "$dir/dir.epub does not exist, please download it to run the analysis."
    fi
done
