#!/bin/bash

# This is slow, do it once.
if [[ ! -e president_calibre.txt ]]; then
    ebook-convert president.epub president_calibre.txt
fi

# The line number where the actual text starts and stops.
start=$(grep -n '^The House Select Committee will come to order' president_calibre.txt | cut -f1 -d: | head -n2 | tail -n 1)
stop=$(grep -n 'For their invaluable assistance in technical matters' president_calibre.txt | cut -f1 -d: | tail -n 1)

sed -n "${start},${stop}p" president_calibre.txt > president.txt
