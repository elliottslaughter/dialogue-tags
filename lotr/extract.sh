#!/bin/bash

rm -f total_count.txt

for kind in basic functional descriptive; do
    while read verb; do
        count=$(rg -c "[,!?]’ ([a-z]*) ?${verb}\b" lotr.txt)
        echo $count $verb
    done < verbs_${kind}.txt | sort -n -r > count_${kind}.txt
    cut -f 1 count_${kind}.txt | awk '{ sum += $1 } END { print "'${kind}'", sum }' >> total_count.txt
done
cat count_*.txt | sort -r -n > all_count.txt

rm -f adverbs.txt
for kind in basic functional descriptive; do
    while read verb; do
        rg -o '[,!?]’ ([a-z]*) ?'"${verb}"' ([^‘’.,;]+|[^,!?]’)*\b[a-z]+ly\b' lotr.txt | rg -v -w '[a-z]*[^l][^y] and [a-z]*[^l][^y]' | rg -v -w 'as' | rg -o -w '[a-z]*ly' >> adverbs.txt
    done < verbs_${kind}.txt
done
sort adverbs.txt | uniq -c | sort -r -n > adverb_count.txt
wc -l adverbs.txt > all_adverb_count.txt


# FIXME: Cannot detect the difference between
# !’ (end of sentence)
# !’ said Name (dialogue tag)
# For now, just disallow


# No Tag, Prefix Tag, or Action Beat:
# Must not contain a postfix dialogue tag
#   * Ends with .’ !’ or ?’ followed by any amount of text as long as it:
#       * Begins with a capital letter and
#       * Doesn't contain ‘
#   * Prefix does not contain ,’ !’ or ?’ (i.e. things that could precede a dialogue tag)
rg -c '^([^’]+|[^,!?”]’)*‘([^’]+|[^,!?”]’)+[.!?]’( [A-Z][^‘]*)?$' lotr.txt > maybe_no_tag_count.txt

# No Tag:
# Must contain exactly one dialogue fragment
#   * Ends with .’ !’ or ?’
#   * Begins with ‘
#   * Interior of line does not contain .’ ,’ !’ or ?’ (i.e. things that could be a break in a quote)
rg -c '^‘([^’]|[^.,!?”]’)*[.!?]’$' lotr.txt > no_count.txt

# Prefix Tag:
# Preceded by dialogue tag and colon
#   * Ends with .’ !’ or ?’ followed by any amount of text as long as it:
#       * Begins with a capital letter and
#       * Doesn't contain ‘
#   * Interior begins with :
#   * Prefix does not contain ,’ !’ or ?’ (i.e. things that could precede a dialogue tag)
rg -c '^([^’]|[^,!?”]’)*: ‘([^’]|[^.,!?”]’)+[.!?]’( [A-Z][^‘]*)?$' lotr.txt > prefix_count.txt

# Action Beat:
# Sequence of dialogue fragments, as long as there are no dialogue tags
#   * Ends with .’ !’ or ?’ followed by non-empty text as long as it:
#       * Begins with a capital letter and
#       * Doesn't contain ‘
#   * Interior does not contain .’ ,’ !’ or ?’ (i.e. things that could precede a dialogue tag or fragment)
#   * Begins with ‘

# Hack: This version is generating false positives because Tolkien
# uses American dialogue tags some of the time (unfortunately).

# rg -c '^‘([^’]|[^.,!?”]’)+[.!?]’ [A-Z][^‘]+$' lotr.txt > action_beat_count.txt
rg -c '^‘([^’]|[^.,!?”]’)+[.!?]’ ([A-EG-RT-Z]|F[^ra]|Fr[^o]|S[^a]|Sa[^m]|Fa[^r]|((Frodo|Sam|Faramir) ([bd-qs-z]|c[^r]|cr[^i]|r[^e]|re[^p]|a[^s]|as[^k])))[^‘]+$' lotr.txt > action_beat_count.txt

# Multiple Action Beats:
# Sequence of dialogue fragments, as long as there are no dialogue tags
#   * Ends with .’ !’ or ?’ followed by any amount of text as long as it:
#       * Begins with a capital letter and
#       * Doesn't contain ‘
#   * Last dialogue fragment begins with ‘ and is not preceded by :
#   * Any number of dialogue fragments bounded by ‘’ as long as they are not preceded by :
rg -c '^((([^’]|[^.,!?”]’)*[^:] )?‘([^’]|[^.,!?”]’)+[.]’)*([^’]|[^.,!?”]’)*[^:] ‘([^’]|[^.,!?”]’)+[.!?]’( [A-Z][^‘]*)?$' lotr.txt > multi_action_beat_count.txt

# FIXME: What about a combination of action beat and prefix tag

# Summary

standard_including_adverbs=$(awk '{ sum += $1 } END { print sum }' count_*.txt)
adverb=$(awk '{ sum += $1 } END { print sum }' adverb_count.txt)
standard=$(( standard_including_adverbs - adverb ))
prefix=$(cat prefix_count.txt)
none=$(cat no_count.txt)
action_beat=$(( $(cat action_beat_count.txt) + $(cat multi_action_beat_count.txt) ))

cat > summary.txt <<EOF
standard $standard
adverb $adverb
prefix $prefix
no $none
action_beat $action_beat
EOF
