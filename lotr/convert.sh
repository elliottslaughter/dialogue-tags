#!/bin/bash

# This is slow, do it once.
if [[ ! -e lotr_calibre.txt ]]; then
    ebook-convert lotr.epub lotr_calibre.txt
fi

# The line number where the actual text starts and stops.
start=$(grep -n 'THE FELLOWSHIP OF THE RING' lotr_calibre.txt | cut -f1 -d: | head -n2 | tail -n 1)
stop=$(grep -n 'APPENDIX A' lotr_calibre.txt | cut -f1 -d: | tail -n 1)

sed -n "${start},${stop}p" lotr_calibre.txt > lotr.txt
