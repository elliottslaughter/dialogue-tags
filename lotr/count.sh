#!/bin/bash

rg -o '[,!?]’\s+[a-z]\w+\s+\w+\b' lotr.txt | rg -o '\b\w+\b' | sort -u | rg '^[a-z]' > verbs_raw.txt

# Manually read through verbs_raw.txt to make verbs.txt
