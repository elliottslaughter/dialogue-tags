#!/bin/bash

# This is slow, do it once.
if [[ ! -e hp1_calibre.txt ]]; then
    ebook-convert hp1.epub hp1_calibre.txt
fi

# The line number where the actual text starts and stops.
start=$(grep -n '^THE BOY WHO LIVED$' hp1_calibre.txt | cut -f1 -d: | head -n2 | tail -n 1)
stop=$(grep -n '^Titles available in the Harry Potter Series' hp1_calibre.txt | cut -f1 -d: | tail -n 1)

sed -n "${start},${stop}p" hp1_calibre.txt > hp1.txt
